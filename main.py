from Formatted_zone.fz_happiness_source import formatted_zone_happiness
from Formatted_zone.fz_world_bank_source import formatted_zone_world_bank
from Trusted_zone.world_bank import trusted_zone_world_bank
from Trusted_zone.happiness_report import trusted_zone_happiness
from Explotation_zone.etl import ETL
from Data_analysis_backbone.analysis import analysis

if __name__ == '__main__':
    formatted_zone_happiness()
    formatted_zone_world_bank()
    trusted_zone_world_bank()
    trusted_zone_happiness()
    ETL()
    analysis()

